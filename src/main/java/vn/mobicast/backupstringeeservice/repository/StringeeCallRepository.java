package vn.mobicast.backupstringeeservice.repository;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import vn.mobicast.backupstringeeservice.domain.StringeeCall;

@ComponentScan(basePackages = {"vn.mobicast.backupstringeeservice.repository"})
@Repository
public interface StringeeCallRepository extends JpaRepository<StringeeCall, Long> {

	@Query("SELECT sc FROM StringeeCall sc WHERE sc.callId = :callId")
	StringeeCall findByCallId (@Param("callId") String callId);
}
