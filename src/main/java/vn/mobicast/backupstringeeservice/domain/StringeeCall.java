package vn.mobicast.backupstringeeservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

import javax.persistence.*;

@Data
@Entity
@Table (name = "stringee_call", schema = "COMPLAIN")
public class StringeeCall {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column (name = "call_id")
	private String callId;
	
	@Column (name = "agent")
	private String agent;
	
	@Column (name = "customer")
	private String customer;
	
	@Column (name = "start_time")
	private Long startTime;
	
	@Column (name = "end_time")
	private Long endTime;
	
	@Column (name = "type")
	private int type;
	
	@Column (name = "status")
	private int status;
	
	@Column (name = "url")
	private String url;
	
	@Column (name = "end_date")
	private Date endDate;
	
	@Column (name = "start_date")
	private Date startDate;
}
