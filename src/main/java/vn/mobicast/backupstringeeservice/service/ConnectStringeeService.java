package vn.mobicast.backupstringeeservice.service;

import java.io.File;
import java.util.List;


import vn.mobicast.backupstringeeservice.dto.StringeeCallDto;

public interface ConnectStringeeService {
	public String getStringeeToken();
	public List<StringeeCallDto> getListCall ( String authToken,String period, int type, boolean isVoiceMail);
	public int getRecord (String authToken, String callId, File file) throws Exception;
}
