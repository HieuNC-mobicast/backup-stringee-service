package vn.mobicast.backupstringeeservice;

import net.sf.log4jdbc.sql.jdbcapi.DataSourceSpy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.jolbox.bonecp.BoneCPDataSource;

import lombok.Data;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Data
@Configuration
@PropertySource({"classpath:/application.properties","classpath:/application.yml"})
@ComponentScan("vn.mobicast.backupstringeeservice")
public class AppConfig {
	@Value("${stringee.email}")
	private String stringeeUser;
	@Value("${stringee.password}")
	private String stringeePassword;
	@Value("${stringee.domain}")
	private String stringeeDomain;
	
	@Value("${root}")
	private String parentOfBackupFileTree;
	@Value("${filebackup.domain}")
	private String fileBackupDomain;
	@Value("${first.parent.tree}")
	private String rootTree;
	
	@Autowired
	DBConfig dbConfig;
	
    @Bean
    public BoneCPDataSource boneCPDataSource() {

        String username = dbConfig.getUserName();
        String password = dbConfig.getPassword();
        BoneCPDataSource boneCPDataSource = new BoneCPDataSource();
        boneCPDataSource.setDriverClass(dbConfig.getDriverName());
        boneCPDataSource.setJdbcUrl(dbConfig.getUrl());
        boneCPDataSource.setUsername(dbConfig.getUserName());
        boneCPDataSource.setPassword(dbConfig.getPassword());
        boneCPDataSource.setIdleConnectionTestPeriodInMinutes(dbConfig.getIdleConnectionTestPeriodInMinutes());
        boneCPDataSource.setIdleMaxAgeInMinutes(dbConfig.getIdleConnectionTestPeriodInMinutes());
        boneCPDataSource.setPoolAvailabilityThreshold(dbConfig.getPoolAvailabilityThreshold());
        boneCPDataSource.setMaxConnectionsPerPartition(dbConfig.getMaxConnectionsPerPartition());
        boneCPDataSource.setMinConnectionsPerPartition(dbConfig.getMinConnectionsPerPartition());
        boneCPDataSource.setPartitionCount(dbConfig.getPartitionCount());
        boneCPDataSource.setAcquireIncrement(dbConfig.getAcquireIncrement());
        boneCPDataSource.setStatementsCacheSize(dbConfig.getStatementsCacheSize());
        boneCPDataSource.setReleaseHelperThreads(dbConfig.getReleaseHelperThreads());
        boneCPDataSource.setConnectionTestStatement(dbConfig.getConnectionTestStatement());
        return boneCPDataSource;

    }

    @Bean
    public EntityManagerFactory entityManagerFactory( BoneCPDataSource BoneCPDataSource) {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(false);
        vendorAdapter.setShowSql(true);
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
//        factory.setPackagesToScan("com.hitex.thirdpartyservice.domain.cus");
        factory.setPackagesToScan("vn.mobicast.backupstringeeservice.domain");
        factory.setDataSource(BoneCPDataSource);
        factory.afterPropertiesSet();

        return factory.getObject();
    }

    @Bean
    public PlatformTransactionManager transactionManager( EntityManagerFactory entityManagerFactory) {

        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory);
        return txManager;
    }
}