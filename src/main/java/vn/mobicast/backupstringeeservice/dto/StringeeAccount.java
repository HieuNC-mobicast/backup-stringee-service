package vn.mobicast.backupstringeeservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import vn.mobicast.backupstringeeservice.AppConfig;

@Data
@AllArgsConstructor
public class StringeeAccount {
	private String email;
	private String password;
	
}
