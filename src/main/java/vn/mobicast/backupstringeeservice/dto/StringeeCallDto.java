package vn.mobicast.backupstringeeservice.dto;


import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import vn.mobicast.backupstringeeservice.domain.StringeeCall;

@Data
public class StringeeCallDto {
	
	@JsonProperty("id")
	private String callId;
	
	@JsonProperty("account_name")
	private String agent;
	
	@JsonProperty("contactname")
	private String customer;
	
	@JsonProperty("start_time")
	private Long startTime;
	
	@JsonProperty("end_time")
	private Long endTime;
	
	@JsonProperty("direction")
	private int type;
	
	@JsonProperty("have_voice_mail")
	private boolean isVoiceMail;
	
	public StringeeCall toStringeeCall () {
		Calendar cal = Calendar.getInstance();
		StringeeCall call = new StringeeCall();
		call.setAgent(this.getAgent());
		call.setCallId(this.getCallId());
		call.setCustomer(this.getCustomer());
		call.setEndTime(this.getEndTime());
		call.setStartTime(this.getStartTime());
		cal.setTimeInMillis(this.getStartTime());
		call.setStartDate(cal.getTime());
		cal.setTimeInMillis(this.getEndTime());
		call.setEndDate(cal.getTime());
		return call;
	}
}
