package vn.mobicast.backupstringeeservice.constant;

public class StringeeConstant {
	public static final int RECORD_SUCCESS = 0;
	public static final int NOT_EXIST_CALL = 2;
	public static final int EXCEPTION_WHEN_RECORD = 1;
	public static final String DELIMITER_DIRECTORY = "/";
}
