package vn.mobicast.backupstringeeservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@Configuration
@PropertySource("classpath:/${env}_config.properties")
public class DBConfig {
	
	@Value("${application.database.cus.url}")
	private String url;
	@Value("${application.database.cus.username}")
	private String userName;
	@Value("${application.database.cus.password}")
	private String password;
	@Value("${application.database.driverName}")
	private String driverName;
	@Value("${application.database.cus.IdleConnectionTestPeriodInMinutes}")
	private int idleConnectionTestPeriodInMinutes;
	@Value("${application.database.cus.IdleMaxAgeInMinutes}")
	private int idleMaxAgeInMinutes;
	@Value("${application.database.cus.PoolAvailabilityThreshold}")
	private int poolAvailabilityThreshold;
	@Value("${application.database.cus.MaxConnectionsPerPartition}")
	private int maxConnectionsPerPartition;
	@Value("${application.database.cus.MinConnectionsPerPartition}")
	private int minConnectionsPerPartition;
	@Value("${application.database.cus.PartitionCount}")
	private int partitionCount;
	@Value("${application.database.cus.AcquireIncrement}")
	private int acquireIncrement;
	@Value("${application.database.cus.StatementsCacheSize}")
	private int statementsCacheSize;
	@Value("${application.database.cus.ReleaseHelperThreads}")
	private int releaseHelperThreads;
	@Value("${application.database.cus.ConnectionTestStatement}")
	private String connectionTestStatement;
}
