package vn.mobicast.backupstringeeservice.serviceImpl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.BufferedSource;
import vn.mobicast.backupstringeeservice.AppConfig;
import vn.mobicast.backupstringeeservice.constant.StringeeConstant;
import vn.mobicast.backupstringeeservice.dto.StringeeCallDto;
import vn.mobicast.backupstringeeservice.dto.StringeeAccount;
import vn.mobicast.backupstringeeservice.service.ConnectStringeeService;
import vn.mobicast.backupstringeeservice.utils.DateUtils;
import vn.mobicast.backupstringeeservice.utils.ListUtils;

@Data
@Service
public class ConnectStringeeImpl implements ConnectStringeeService{
	
	@Autowired
	private AppConfig appConfig;
	
	public static String PREFIX = "https://api.stringeex.com/v1";
	public static String STRINGEE_LOGIN = "/account";
	public static String STRINGEE_CALL_HISTORY = "/call/history";
	public static String STRINGEE_CALL_RECORD = "/recordfile/play";
	
	@Override
	public String getStringeeToken() {
//		WebClient connector = WebClient.create(PREFIX);
		try {
//			BodyInserter<StringeeAccount, ReactiveHttpOutputMessage> inserter = BodyInserters.fromValue();
//			String res = connector.post()
//					.uri(STRINGEE_LOGIN)
//					.body(inserter)
//					.retrieve()
//					.bodyToMono(String.class)
//					.block();
			//
			ObjectMapper mapper = new ObjectMapper();
			StringeeAccount stringeeAccount = new StringeeAccount(appConfig.getStringeeUser(), appConfig.getStringeePassword());
			RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), mapper.writeValueAsString(stringeeAccount));
			OkHttpClient client = new OkHttpClient();
			client = client.newBuilder().readTimeout(3, TimeUnit.MINUTES).build();
			Request request = new Request.Builder().post(requestBody).url(PREFIX + STRINGEE_LOGIN)
					.build();
			Response rs = client.newCall(request).execute();
			String res = rs.body().string();
			rs.close();
			JsonNode root = mapper.readTree(res);
			String token = root.get("portals").get(0).get("auth_token").asText();
			return token;
		} catch (Exception e) {
			return "Error";
		}
	}
	
	@Override
	public List<StringeeCallDto> getListCall ( String stringeeAuthToken,String period, int type, boolean isVoiceMail) {
		try {
			if (period == null) {
				throw new Exception("miss period param");
			}
			String url = STRINGEE_CALL_HISTORY + "?time=" + period;
			if(isVoiceMail) {
				url += "&has_voicemail=1";
			}else {
				url += "&has_voicemail=0";
			}
			url += "&status=1";
			if (!isVoiceMail && (type == 0 || type == 1)) {
				url += ("&direction=" + type);
			}
			OkHttpClient client = new OkHttpClient();
			client = client.newBuilder().readTimeout(3, TimeUnit.MINUTES).build();
			Request request = new Request.Builder().get().url(PREFIX + url)
					.addHeader("X-STRINGEE-AUTH",stringeeAuthToken)
					.build();
			Response rs = client.newCall(request).execute();
			String response = rs.body().string();
			rs.close();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			JsonNode rootNode = mapper.readTree(response);
			String arrayCallAsString = mapper.writeValueAsString(rootNode.get("data").get("rows"));
			List<StringeeCallDto> res = Arrays.asList(mapper.readValue(arrayCallAsString, StringeeCallDto[].class));
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null ;
		}
	}
	
	public StringeeCallDto getCallById (String callId, String stringeeAuthToken) {
		if (StringUtils.isEmpty(callId)) {
			return null;
		}
		String url = STRINGEE_CALL_HISTORY + "?call_id=" + callId;
		try {
			OkHttpClient client = new OkHttpClient();
			client = client.newBuilder().readTimeout(3, TimeUnit.MINUTES).build();
			Request request = new Request.Builder().get().url(PREFIX + url)
					.addHeader("X-STRINGEE-AUTH",stringeeAuthToken)
					.build();
			Response rs = client.newCall(request).execute();
			String response = rs.body().string();
			rs.close();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			JsonNode rootNode = mapper.readTree(response);
			String arrayCallAsString = mapper.writeValueAsString(rootNode.get("data").get("rows"));
			List<StringeeCallDto> res = Arrays.asList(mapper.readValue(arrayCallAsString, StringeeCallDto[].class));
			return ListUtils.isEmpty(res) ? null : res.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
	}
	@Override
	public int getRecord (String authToken, String callId, File file) throws Exception {
		if(StringUtils.isEmpty(callId)) {
			return StringeeConstant.NOT_EXIST_CALL;
		}
//		File file = this.getFile();
//		if (file.exists()) {
//			System.out.println("exist file");
//			return;
//		}
		
		try {
			String url = STRINGEE_CALL_RECORD + "?callId=" + callId;
			OkHttpClient client = new OkHttpClient();
			client = client.newBuilder().readTimeout(3, TimeUnit.MINUTES).build();
			Request request = new Request.Builder().get().url(PREFIX + url)
					.addHeader("X-STRINGEE-AUTH",authToken)
					.addHeader("Content-Type", "application/mp3")
					.addHeader("Accept", "application/mp3")
					.build();
			Response response = client.newCall(request).execute();
			String contentType = response.header("Content-Type");
			if (!contentType.equals("application/mp3")) {
				return StringeeConstant.NOT_EXIST_CALL;
			}
			InputStream stream = response.body().byteStream();
			BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(file));
			try {
				byte[] data = new byte[1024];
				int count;
				int total = 0;
				while ((count = stream.read(data)) != -1) {
					total += count;
					os.write(data,0,count);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return StringeeConstant.EXCEPTION_WHEN_RECORD;
			}
			finally {
				stream.close();
				os.close();
			}
			return StringeeConstant.RECORD_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return StringeeConstant.EXCEPTION_WHEN_RECORD;
		}
	}
}
