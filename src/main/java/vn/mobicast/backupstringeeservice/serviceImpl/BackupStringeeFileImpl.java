package vn.mobicast.backupstringeeservice.serviceImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.aspectj.apache.bcel.generic.TargetLostException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import lombok.Data;
import vn.mobicast.backupstringeeservice.AppConfig;
import vn.mobicast.backupstringeeservice.constant.StringeeConstant;
import vn.mobicast.backupstringeeservice.domain.StringeeCall;
import vn.mobicast.backupstringeeservice.dto.StringeeCallDto;
import vn.mobicast.backupstringeeservice.repository.StringeeCallRepository;
import vn.mobicast.backupstringeeservice.utils.DateUtils;
import vn.mobicast.backupstringeeservice.utils.ListUtils;

@Data
@Service
public class BackupStringeeFileImpl {

	private static final String MP3 = ".mp3";
	private static final String OTHER = "other";
	private static final String CALL_IN = "call_in";
	private static final String CALL_OUT = "call_out";
	private static final String VOICE_MAIL = "voice_mail";
	private static final int SUCCESS = 0;
	private static final int FAIL = 1;
	
	@Autowired
	ConnectStringeeImpl connectStringeeService;
	
	@Autowired
	AppConfig appConfig;
	
	@Autowired
	StringeeCallRepository stringeeCallRepository;
	
	public Object backUpStringeeFileYesterday () {
		Date today = Calendar.getInstance().getTime();
		String stringeeAuthToken = connectStringeeService.getStringeeToken();
		List<StringeeCallDto> listCallIn = null;
		List<StringeeCallDto> listCallOut = null;
		List<StringeeCallDto> listVoiceMail = null;
		List<StringeeCallDto> listCallOutTwoDayBefore = null;
		List<StringeeCallDto> listCallInTwoDayBefore = null;
		List<StringeeCallDto> listVoiceMailTwoDayBefore = null;
		List<StringeeCallDto> listCallOutRemain = null;
		List<StringeeCallDto> listCallInRemain = null;
		List<StringeeCallDto> listVoiceMailRemain = null;
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, (cal.get(Calendar.DAY_OF_MONTH) - 1));
		Date yesterday = cal.getTime();
		cal.set(Calendar.DAY_OF_MONTH, (cal.get(Calendar.DAY_OF_MONTH) - 1));
		Date ereyesterday = cal.getTime();
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		Long ereMidNight = cal.getTimeInMillis();
		String yesterdayPeriod = DateUtils.getDurationOfDaysInLongFormat(yesterday, yesterday);
		String twoDayBeforePeriod = DateUtils.getDurationOfDaysInLongFormat(ereyesterday, yesterday);
		try {
			listCallIn = connectStringeeService.getListCall(stringeeAuthToken, yesterdayPeriod, 1, false);
		} catch (Exception e) {
			handleException(e);
		}
		try {
			listCallInTwoDayBefore = connectStringeeService.getListCall(stringeeAuthToken, twoDayBeforePeriod, 1, false);
		} catch (Exception e) {
			handleException(e);
		}
		try {
			listCallOut = connectStringeeService.getListCall(stringeeAuthToken, yesterdayPeriod, 0, false);
		} catch (Exception e) {
			handleException(e);
		}
		try {
			listCallOutTwoDayBefore = connectStringeeService.getListCall(stringeeAuthToken, twoDayBeforePeriod, 0, false);
		} catch (Exception e) {
			handleException(e);
		}
		try {
			listVoiceMail = connectStringeeService.getListCall(stringeeAuthToken, yesterdayPeriod, -1, true);
		} catch (Exception e) {
			handleException(e);
		}
		try {
			listVoiceMailTwoDayBefore = connectStringeeService.getListCall(stringeeAuthToken, twoDayBeforePeriod, -1, true);
		} catch (Exception e) {
			handleException(e);
		}
		
		if(!ListUtils.isEmpty(listCallInTwoDayBefore)) {
			listCallInRemain = listCallInTwoDayBefore.stream()
					.filter(call -> call.getStartTime() < ereMidNight && call.getEndTime() > ereMidNight)
					.collect(Collectors.toList());
		}
		if(!ListUtils.isEmpty(listCallOutTwoDayBefore)) {
			listCallInRemain = listCallOutTwoDayBefore.stream()
					.filter(call -> call.getStartTime() < ereMidNight && call.getEndTime() > ereMidNight)
					.collect(Collectors.toList());
		}
		if(!ListUtils.isEmpty(listVoiceMailTwoDayBefore)) {
			listVoiceMailRemain = listCallInTwoDayBefore.stream()
					.filter(call -> call.getStartTime() < ereMidNight && call.getEndTime() > ereMidNight)
					.collect(Collectors.toList());
		}
		List<StringeeCallDto> listCall = ListUtils.mergeList(listCallIn, listCallOut, listVoiceMail, listCallInRemain, listCallOutRemain, listVoiceMailRemain);
		if (ListUtils.isEmpty(listCall)) {
			return null;
		}
		List<StringeeCall> listHistoryCall = new ArrayList<>();
		
		for (StringeeCallDto call : listCall) {
			StringeeCall stringeeCall = this.backUpCall(call, today, stringeeAuthToken);
			if (stringeeCall != null) {
				listHistoryCall.add(stringeeCall);
			}
		}
		return this.saveHistoryCall(listHistoryCall);
	}
	
	public Object backUpFollowingPeriod (Date dateFrom, Date dateTo) {
		String stringeeAuthToken = connectStringeeService.getStringeeToken();
		Date today = Calendar.getInstance().getTime();
		List<StringeeCallDto> listCallIn = null;
		List<StringeeCallDto> listCallOut = null;
		List<StringeeCallDto> listVoiceMail = null;
		String duration = DateUtils.getDurationOfDaysInLongFormat(dateFrom, dateTo);
		try {
			listCallIn = connectStringeeService.getListCall(stringeeAuthToken, duration, 1, false);
		} catch (Exception e) {
			handleException(e);
		}
		try {
			listCallOut = connectStringeeService.getListCall(stringeeAuthToken, duration, 0, false);
		} catch (Exception e) {
			handleException(e);
		}
		try {
			listVoiceMail = connectStringeeService.getListCall(stringeeAuthToken, duration, -1, true);
		} catch (Exception e) {
			handleException(e);
		}
		List<StringeeCallDto> listCall = ListUtils.mergeList(listCallIn, listCallOut, listVoiceMail);
		List<StringeeCall> listHistoryCall = new ArrayList<>();
		
		for (StringeeCallDto call : listCall) {
			StringeeCall stringeeCall = this.backUpCall(call, today, stringeeAuthToken);
			if (stringeeCall != null) {
				listHistoryCall.add(stringeeCall);
			}
		}
		return this.saveHistoryCall(listHistoryCall);
	}
	
	public Object backUpManuall (List<String> listCallId) {
		if (StringUtils.isEmpty(listCallId)) {
			return null;
		}
		String stringeeAuthToken = connectStringeeService.getStringeeToken();
		Date today = Calendar.getInstance().getTime();
		List<StringeeCall> listCall = new ArrayList<>();
		for (String callId : listCallId) {
			StringeeCallDto call = connectStringeeService.getCallById(callId, stringeeAuthToken);
			StringeeCall stringeeCall = this.backUpCall(call, today, stringeeAuthToken);
			StringeeCall origin = stringeeCallRepository.findByCallId(callId);
			if(origin != null) {
				stringeeCall.setId(origin.getId());
			}
			listCall.add(stringeeCall);
		}
		return this.saveHistoryCall(listCall);
	}
	
	public List<StringeeCall> saveHistoryCall (List<StringeeCall> listCall) {
		return stringeeCallRepository.saveAll(listCall);
	}
	
	public String getLocationFileBackupCall(StringeeCallDto call, Date today) {
		Calendar cal = Calendar.getInstance();
		if (call == null || call.getStartTime() == null || call.getStartTime() <= 0) {
			if (today != null) cal.setTime(today);
		}else {
			cal.setTimeInMillis(call.getStartTime());
		}
		String root = this.getRoot();
		String type = OTHER;
		if (call.isVoiceMail()) {
			type = VOICE_MAIL;
		}else {
			if (call.getType() == 1) {
				type = CALL_IN;
			}
			if (call.getType() == 0) {
				type = CALL_OUT;
			}
		}
		String path = root + StringeeConstant.DELIMITER_DIRECTORY + cal.get(Calendar.YEAR)
				+ StringeeConstant.DELIMITER_DIRECTORY + (cal.get(Calendar.MONTH) + 1)
				+ StringeeConstant.DELIMITER_DIRECTORY + cal.get(Calendar.DAY_OF_MONTH )
				+ StringeeConstant.DELIMITER_DIRECTORY + type
				+ StringeeConstant.DELIMITER_DIRECTORY + call.getCallId()
				+ MP3;
		return path;
	}
	public String getRoot() {
		
		return appConfig.getParentOfBackupFileTree() + appConfig.getRootTree();
	}
	public StringeeCall backUpCall (StringeeCallDto call,  Date today, String stringeeAuthToken) {
		File file = new File(this.getLocationFileBackupCall(call,today));
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		int result = 0;
		try {
			result = connectStringeeService.getRecord(stringeeAuthToken, call.getCallId(), file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (result == StringeeConstant.RECORD_SUCCESS || result == StringeeConstant.EXCEPTION_WHEN_RECORD) {
			StringeeCall stringeeCall = call.toStringeeCall();
			String absolutePath = file.getAbsolutePath();
			String relativePath = absolutePath.substring(appConfig.getParentOfBackupFileTree().length());
			stringeeCall.setUrl(appConfig.getFileBackupDomain() + relativePath);
			stringeeCall.setStatus(result);
			int type = 0; //wrong value to check
			if (call.isVoiceMail()) {
				type = 3; //voice-mail
			}else {
				if (call.getType() == 1) type = 2; //call-in
				if (call.getType() == 0) type = 1; //call-out
			}
			stringeeCall.setType(type);
			return stringeeCall;
		} 
		return null;
	}
	
	
	public void handleException(Exception e) {
		//do something here
	}
	
	
}
