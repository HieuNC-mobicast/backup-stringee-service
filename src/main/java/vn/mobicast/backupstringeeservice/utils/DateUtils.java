package vn.mobicast.backupstringeeservice.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	public static String getDurationOfDaysInLongFormat(Date dateFrom, Date dateTo) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateFrom);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		String start = String.valueOf(cal.getTimeInMillis());
		cal.setTime(dateTo);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		String end = String.valueOf(cal.getTimeInMillis());
		return start + "-" + end;
	}
	public static Date getDateFromUnixTime (Long miliseconds) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(miliseconds);
		return cal.getTime();
	}
}
