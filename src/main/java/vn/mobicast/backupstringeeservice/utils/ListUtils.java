package vn.mobicast.backupstringeeservice.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {
	public static boolean isEmpty(List list) {
		return list == null || list.isEmpty();
	}
	
	public static <E> List<E> mergeList(List<E> ...lists ) {
		  List<E> listMerge = new ArrayList<>();
		  for (List list : lists) {
			  if (!isEmpty(list)) {
				  listMerge.addAll(list);
			  }
		  }
		  return listMerge;
	}
}
