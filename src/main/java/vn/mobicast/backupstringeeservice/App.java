package vn.mobicast.backupstringeeservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.StringUtils;

import vn.mobicast.backupstringeeservice.serviceImpl.BackupStringeeFileImpl;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        BackupStringeeFileImpl backupService = context.getBean(BackupStringeeFileImpl.class);
        if (args == null || args.length == 0) {
        	backupService.backUpStringeeFileYesterday();
        }else {
        	if (args[0].equals("gd") && args.length == 3) {
            	try {
    				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    				Date dateFrom = format.parse(args[1]);
    				Date dateTo = format.parse(args[2]);
    				Date dateAfterOneDay;
    				int count = 0;
    				if (dateTo.before(dateFrom)) {
    					System.exit(0);
    				}
    				Calendar cal = Calendar.getInstance();
    				do {
						cal.setTime(dateFrom);
						cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 1);
						dateAfterOneDay = cal.getTime();
						backupService.backUpFollowingPeriod(dateFrom, dateAfterOneDay);
						dateFrom = dateAfterOneDay;
						count++;
					} while (dateAfterOneDay.before(dateTo) && count < 200);
    				
    				
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
            }
            if (args[0].equals("i") && args.length > 1) {
            	List<String> listCallId = new ArrayList<>();
            	for (int i = 1; i < args.length; i++) {
            		listCallId.add(args[i]);
            	}
            	backupService.backUpManuall(listCallId);
            }
        }
        System.exit(0);
    }
}